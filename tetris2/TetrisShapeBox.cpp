#include "TetrisShapeBox.h"

void TetrisShapeBox::start() {
	std::shared_ptr<TetrisShape> sharedThis = shared_from_this();
	sf::Color colour = sf::Color(255, 255, 68);

	rotation = 0;

	std::shared_ptr<TetrisShapeElement> shapepointer1(new TetrisShapeElement(sharedThis, colour, texture, position[0], position[1]));
	std::shared_ptr<TetrisShapeElement> shapepointer2(new TetrisShapeElement(sharedThis, colour, texture, position[0], position[1] + 1));
	std::shared_ptr<TetrisShapeElement> shapepointer3(new TetrisShapeElement(sharedThis, colour, texture, position[0] + 1, position[1]));
	std::shared_ptr<TetrisShapeElement> shapepointer4(new TetrisShapeElement(sharedThis, colour, texture, position[0] + 1, position[1] + 1));

	shapeElements.push_back(shapepointer1);
	shapeElements.push_back(shapepointer2);
	shapeElements.push_back(shapepointer3);
	shapeElements.push_back(shapepointer4);
}

bool TetrisShapeBox::rotate(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	return true; //the box never rotates
}