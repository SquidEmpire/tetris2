#include <iostream>
#include <chrono>
#include <vector>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "config.h"
#include "TetrisDrawable.h"
#include "TetrisUIRectangle.h"
#include "TetrisUIText.h"
#include "TetrisShapeElement.h"
#include "TetrisShape.h"
#include "TetrisShapeL.h"
#include "TetrisShapeBar.h"
#include "TetrisShapeBox.h"
#include "TetrisShapeSquiggle.h"
#include "TetrisShapeT.h"

typedef std::chrono::high_resolution_clock Clock;

//remove a drawable from the vector of drawable objects (note that removing from vectors is inefficient) https://stackoverflow.com/a/15998752
void removeElementFromDrawables(std::vector<std::shared_ptr<TetrisDrawable>>& drawableObjects, std::shared_ptr<TetrisDrawable> drawable) {
    auto indexOfElementInDrawables = std::find(drawableObjects.begin(), drawableObjects.end(), drawable);
    if (indexOfElementInDrawables != drawableObjects.end()) {
        drawableObjects.erase(indexOfElementInDrawables);
    }
}

//builds the UI header and returns a pointer to the "cleared rows" text element for later updating
std::shared_ptr<TetrisUIText> buildUi(std::vector<std::shared_ptr<TetrisDrawable>>& drawableObjects, sf::Font titleFont, sf::Font textFont) {
    sf::Color uiBackgroundColour = sf::Color(14, 24, 29);
    std::shared_ptr<TetrisUIRectangle>rectanglePointer(new TetrisUIRectangle(uiBackgroundColour, 0, 0, config::windowWidth, config::windowHeight - config::gridRawHeight));
    std::shared_ptr<TetrisUIText>titlePointer(new TetrisUIText(titleFont, "Tetris 2", 24, 10, 12));
    std::shared_ptr<TetrisUIText>textPointer(new TetrisUIText(textFont, "Cleared rows: 0", 12, 150, 24));
    drawableObjects.push_back(rectanglePointer);
    drawableObjects.push_back(titlePointer);
    drawableObjects.push_back(textPointer);
    return textPointer;
}

struct GameOverUI {
    std::shared_ptr<TetrisUIText> titlePointer;
    std::shared_ptr<TetrisUIText> subtitlePointer;
};
//show a big game over screen
GameOverUI displayGameOver(std::vector<std::shared_ptr<TetrisDrawable>>& drawableObjects, sf::Font titleFont, sf::Font textFont) {
    std::shared_ptr<TetrisUIText>titlePointer(new TetrisUIText(
        titleFont, "Game over!", 48, 5, config::windowHeight/2
    )); //center text
    std::shared_ptr<TetrisUIText>subtitlePointer(new TetrisUIText(
        textFont, "Press enter to restart", 24, 5, (config::windowHeight / 2) + 48
    ));
    drawableObjects.push_back(titlePointer);
    drawableObjects.push_back(subtitlePointer);
    return GameOverUI{
        titlePointer,
        subtitlePointer
    };
}

void removeGameOver(std::vector<std::shared_ptr<TetrisDrawable>>& drawableObjects, GameOverUI uiElements) {
    removeElementFromDrawables(drawableObjects, uiElements.titlePointer);
    removeElementFromDrawables(drawableObjects, uiElements.subtitlePointer);
}

//to be called after clearing a row, move the entire board above the given row down by one element
void shiftGameGridDown(std::shared_ptr<TetrisShapeElement> (&gameGrid)[config::gridWidth][config::gridHeight], int rowToShiftTo) {
    std::shared_ptr<TetrisShapeElement> shapeElement;
    for (int col = config::gridWidth - 1; col >= 0; col--) {
        for (int row = rowToShiftTo - 1; row >= 0; row--) {
            shapeElement = gameGrid[col][row];
            if (shapeElement != nullptr) {
                shapeElement->moveToPosition(col, row + 1, gameGrid);
            }
        }
    }
}

//given a row index clear all the shape elements on the row
void clearRow(int row, std::shared_ptr<TetrisShapeElement> (&gameGrid)[config::gridWidth][config::gridHeight], std::vector<std::shared_ptr<TetrisDrawable>>& drawableObjects) {
    std::shared_ptr<TetrisShapeElement> shapeElement;
    for (int col = 0; col < config::gridWidth; col++) {
        shapeElement = gameGrid[col][row];
        if (shapeElement != nullptr) {
            if (shapeElement->parent != nullptr) {
                shapeElement->parent->removeShapeElement(shapeElement);
            }           
            removeElementFromDrawables(drawableObjects, shapeElement);

            //remove the shape from the gamegrid
            gameGrid[col][row] = nullptr;
            shapeElement = nullptr;
        }
    }
}

//go through the grid row by row and detect and clear any full rows, returns the number of rows cleared
int clearFullRows(std::shared_ptr<TetrisShapeElement> (&gameGrid)[config::gridWidth][config::gridHeight], std::vector<std::shared_ptr<TetrisDrawable>>& drawableObjects) {
    bool rowIsFull;
    int rowsCleared = 0;
    for (int row = 0; row < config::gridHeight; row++) {
        rowIsFull = true;
        for (int col = 0; col < config::gridWidth; col++) {
            if (gameGrid[col][row] == nullptr) {
                //row isn't full, abord this row
                rowIsFull = false;
                break;
            }
        }
        if (rowIsFull) {
            clearRow(row, gameGrid, drawableObjects);
            shiftGameGridDown(gameGrid, row);
            rowsCleared++;
        }
    }
    return rowsCleared;
}

std::shared_ptr<TetrisShape> createShape(sf::Texture shapeElementTexture) {
    //pick a random shape
    int shapeIndex = rand() % 5;
    switch (shapeIndex) {
        case 0:
            return std::shared_ptr<TetrisShape>(new TetrisShapeL(shapeElementTexture));
        case 1:
            return std::shared_ptr<TetrisShape>(new TetrisShapeBar(shapeElementTexture));
        case 2:
            return std::shared_ptr<TetrisShape>(new TetrisShapeBox(shapeElementTexture));
        case 3:
            return std::shared_ptr<TetrisShape>(new TetrisShapeSquiggle(shapeElementTexture));
        case 4:
            return std::shared_ptr<TetrisShape>(new TetrisShapeT(shapeElementTexture));
        default:
            return std::shared_ptr<TetrisShape>(new TetrisShapeL(shapeElementTexture));
    }
}

//struct for info to be returned after dropping a row
struct ShapeDropResults {
    bool shapeDropped;
    bool gameOver;
    int rowsCleared;
};
//try to drop the active shape down by 1 space. If it fails, either end the game or create a new active shape, deactivate the old one and check for rows to be cleared.
ShapeDropResults dropActiveShape(
    std::shared_ptr<TetrisShape> &activeShape,
    std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight],
    std::vector<std::shared_ptr<TetrisDrawable>> &drawableObjects,
    sf::Texture shapeElementTexture
    ) {
    bool shapeDropped = false;
    bool gameOver = false;
    int rowsCleared = 0;
    ShapeDropResults results;
    if (activeShape->isActive) {
        activeShape->fall(gameGrid);
        shapeDropped = true;
    } else {
        //the active shape has deactivated (stopped moving) check if the shape is at the top of the screen        
        if (activeShape->position[1] == 0) {
            //the shape was at the top of the screen, game over!     
            gameOver = true;
        } else {
            //check for full rows and make a new active shape
            rowsCleared = clearFullRows(gameGrid, drawableObjects);
            activeShape = createShape(shapeElementTexture);
            activeShape->start();
            drawableObjects.push_back(activeShape);
        }
    }
    results.shapeDropped = shapeDropped;
    results.gameOver = gameOver;
    results.rowsCleared = rowsCleared;
    return results;
}

//struct for info to be returned from game initialisation
struct InitialGameValues {
    std::shared_ptr<TetrisUIText> textPointer;
    std::shared_ptr<TetrisShape> shapepointer;
};

InitialGameValues initialiseGame(
    std::vector<std::shared_ptr<TetrisDrawable>>& drawableObjects,
    sf::Texture shapeElementTexture,
    sf::Font titleFont,
    sf::Font textFont
    ) {
    InitialGameValues values;
    //add the UI
    values.textPointer = buildUi(drawableObjects, titleFont, textFont);

    //add the initial shape
    std::shared_ptr<TetrisShape> shapepointer = createShape(shapeElementTexture);
    drawableObjects.push_back(shapepointer);
    values.shapepointer = shapepointer;
    return values;
}

std::shared_ptr<TetrisShape> RestartGame(
    std::shared_ptr<TetrisShape> activeShape,
    std::vector<std::shared_ptr<TetrisDrawable>>& drawableObjects,
    std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight],
    sf::Texture shapeElementTexture,
    int &rowsCleared,
    GameOverUI gameOverUI
    ){
    //the active shape may not be on the game grid (but still drawn) when the restart goes through
    removeElementFromDrawables(drawableObjects, activeShape);
    //clear every row of the game board
    for (int row = 0; row < config::gridHeight; row++) {
        clearRow(row, gameGrid, drawableObjects);
    }
    //reset the total rows cleared
    rowsCleared = 0;
    //close the game over UI
    removeGameOver(drawableObjects, gameOverUI);
    //spawn a new shape
    std::shared_ptr<TetrisShape> shapepointer = createShape(shapeElementTexture);
    drawableObjects.push_back(shapepointer);
    return shapepointer;
}

//This is called each frame to clear the view and redraw all the things to be drawn
void render(sf::RenderWindow& window, std::vector<std::shared_ptr<TetrisDrawable>>& drawableObjects) {
    window.clear();
    for (std::shared_ptr<TetrisDrawable> object : drawableObjects) {
        object->draw(window);
    }
    window.display();
}

int main() {
    //the texture to use on the tetris shapes
    sf::Texture shapeElementTexture;
    shapeElementTexture.loadFromFile("assets/texture.png");

    //the font to use for the UI
    sf::Font titleFont;
    titleFont.loadFromFile("assets/KarmaFuture.ttf");
    sf::Font textFont;
    textFont.loadFromFile("assets/consola.ttf");

    //the music!
    sf::Music music;
    music.openFromFile("assets/music.ogg");
    music.setLoop(true);
    music.setVolume(50);
    music.play();

    //vector of all objects that are to be drawn each frame
    std::vector<std::shared_ptr<TetrisDrawable>> drawableObjects;
    //the window to draw to
    sf::RenderWindow window(sf::VideoMode(config::windowWidth, config::windowHeight), "Tetris 2");
    //the grid of all the spaces on the gamefield
    std::shared_ptr<TetrisShapeElement> gameGrid[config::gridWidth][config::gridHeight];

    //the total number of rows cleared
    int rowsCleared = 0;
    //whether the game is still going
    bool gameOver = false;

    //the shape that is currently falling and is controlled by the player
    std::shared_ptr<TetrisShape> activeShape;
    //the textelement that shows the number of rows we have cleared on the UI
    std::shared_ptr<TetrisUIText> rowsClearedText;
    //the struct that contains the game over UI elements
    GameOverUI gameOverUI = { nullptr, nullptr };

    InitialGameValues initialisationValues;
    initialisationValues = initialiseGame(drawableObjects, shapeElementTexture, titleFont, textFont);
    activeShape = initialisationValues.shapepointer;
    rowsClearedText = initialisationValues.textPointer;
    activeShape->start();

    auto timeOfLastFall = Clock::now();
    ShapeDropResults shapeDropResults = { false, false, 0 };
    while (window.isOpen()) {

        auto time = Clock::now();
        long delta = std::chrono::duration_cast<std::chrono::milliseconds>(time - timeOfLastFall).count();

        //listen to events (user input)
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (event.type == sf::Event::KeyPressed) {
                switch (event.key.code) {
                    case sf::Keyboard::Left:
                        activeShape->moveLeft(gameGrid);
                        break;
                    case sf::Keyboard::Right:
                        activeShape->moveRight(gameGrid);
                        break;
                    case sf::Keyboard::Up:
                        activeShape->rotate(gameGrid);
                        break;
                    case sf::Keyboard::Down:
                        shapeDropResults = dropActiveShape(activeShape, gameGrid, drawableObjects, shapeElementTexture);
                        if (shapeDropResults.rowsCleared > 0) {
                            rowsCleared += shapeDropResults.rowsCleared;
                            rowsClearedText->updateText("Cleared rows : " + std::to_string(rowsCleared));
                        }
                        break;
                    case sf::Keyboard::Enter:
                        //restart
                        activeShape = RestartGame(activeShape, drawableObjects, gameGrid, shapeElementTexture, rowsCleared, gameOverUI);
                        activeShape->start();
                        rowsClearedText->updateText("Cleared rows : " + std::to_string(rowsCleared));
                        shapeDropResults = { false, false, 0 };
                        gameOver = false;
                        break;
                }
            }
        }

        //each interval specified by gameSpeed we automatically drop the shape
        if (delta >= config::gameSpeed) {
            timeOfLastFall = Clock::now();
            if (activeShape != nullptr) {
                shapeDropResults = dropActiveShape(activeShape, gameGrid, drawableObjects, shapeElementTexture);
                if (shapeDropResults.rowsCleared > 0) {
                    rowsCleared += shapeDropResults.rowsCleared;
                    rowsClearedText->updateText("Cleared rows : " + std::to_string(rowsCleared));
                }
            }
        }

        if (shapeDropResults.gameOver && !gameOver) {
            gameOver = true;
            gameOverUI = displayGameOver(drawableObjects, titleFont, textFont);
        }

        render(window, drawableObjects);
        
    }

    return 0;

}