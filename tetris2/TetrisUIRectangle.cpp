#include "TetrisUIRectangle.h"

TetrisUIRectangle::TetrisUIRectangle(sf::Color colour, int x, int y, int width, int height) {
	TetrisUIRectangle::position[0] = x;
	TetrisUIRectangle::position[1] = y;
	TetrisUIRectangle::rectangle.setSize(sf::Vector2f(width, height));
	TetrisUIRectangle::rectangle.setPosition(position[0], position[1]);
	TetrisUIRectangle::rectangle.setFillColor(colour);
};

void TetrisUIRectangle::draw(sf::RenderWindow& window) {
	TetrisUIRectangle::rectangle.setPosition(position[0], position[1]);
	window.draw(TetrisUIRectangle::rectangle);
};