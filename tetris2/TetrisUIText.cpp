#include <SFML/Graphics.hpp>
#include "TetrisUIText.h"

TetrisUIText::TetrisUIText(sf::Font &font, std::string content, int size, int x, int y) {
	TetrisUIText::font = font;
	TetrisUIText::text.setFont(TetrisUIText::font);
	TetrisUIText::text.setString(content);
	TetrisUIText::text.setCharacterSize(size); //pixels
	TetrisUIText::text.setFillColor(sf::Color::White);
	TetrisUIText::position[0] = x;
	TetrisUIText::position[1] = y;
}

void TetrisUIText::updateText(std::string content) {
	TetrisUIText::text.setString(content);
}

void TetrisUIText::draw(sf::RenderWindow& window) {
	text.setPosition(TetrisUIText::position[0], TetrisUIText::position[1]);
	window.draw(TetrisUIText::text);
}