#pragma once
#include "TetrisDrawable.h"
class TetrisUIRectangle : public TetrisDrawable
{
	public:
		TetrisUIRectangle(sf::Color colour, int x, int y, int width, int height);
		void draw(sf::RenderWindow& window);
	private:
		sf::RectangleShape rectangle;
		int position[2];
};


