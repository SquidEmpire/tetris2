#include <iostream>
#include <SFML/Graphics.hpp>
#include "config.h"
#include "TetrisShapeElement.h"
#include "TetrisShape.h"

TetrisShape::TetrisShape(sf::Texture &texture) {
	isActive = true;
	position[0] = rand() % (config::gridWidth - 2); //allow shapes to appear along the top of the grid (-2 because that's the max width of any shape)
	position[1] = 0; //Note that once this shape is deactivated this position may no longer be accurate
	rotation = 0;
	TetrisShape::texture = texture;
}

void TetrisShape::draw(sf::RenderWindow& window) {
	for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
		if (shapeElement != nullptr) {
			shapeElement->draw(window);
		}
	}
}

void TetrisShape::removeShapeElement(std::shared_ptr<TetrisShapeElement> elementToRemove) {
	for (std::shared_ptr<TetrisShapeElement> &shapeElement : shapeElements) {
		if (shapeElement == elementToRemove) {
			shapeElement = nullptr;
		}
	}
}

bool TetrisShape::fall(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	if (!isActive) {
		return false;
	}
	int newYPosition = position[1] + 1;
	bool canFall = true;
	//first check that all our elements are clear to fall
	for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
		canFall = canFall && shapeElement->canFall(gameGrid);
	}
	//if we can't fall deactivate the shape (we hit something)
	if (!canFall) {
		isActive = false;
		return false;
	}
	//otherwise, fall
	for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
		shapeElement->fall(gameGrid);
	}
	position[1] = newYPosition;
	return true;
}

bool TetrisShape::moveLeft(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	if (!isActive) {
		return false;
	}
	int newXPosition = position[0] - 1;
	bool canMove = true;
	//first check that all our elements are clear to move
	for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
		canMove = canMove && shapeElement->canMoveLeft(gameGrid);
	}
	//if we can move, move
	if (canMove) {
		for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
			shapeElement->moveLeft(gameGrid);
		}
		position[0] = newXPosition;
	}
	return canMove;
}

bool TetrisShape::moveRight(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	if (!isActive) {
		return false;
	}
	int newXPosition = position[0] + 1;
	bool canMove = true;
	//first check that all our elements are clear to move
	for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
		canMove = canMove && shapeElement->canMoveRight(gameGrid);
	}
	//if we can move, move
	if (canMove) {
		for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
			shapeElement->moveRight(gameGrid);
		}
		position[0] = newXPosition;
	}
	return canMove;
}