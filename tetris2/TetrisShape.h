#pragma once
class TetrisShape : public TetrisDrawable, public std::enable_shared_from_this<TetrisShape>
{
	public:
		TetrisShape(sf::Texture &texture);
		bool isActive;
		int position[2];
		int rotation; //can be 0-3
		
		void draw(sf::RenderWindow& window);
		void removeShapeElement(std::shared_ptr<TetrisShapeElement> elementToRemove);

		bool fall(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
		bool moveLeft(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
		bool moveRight(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);

		virtual void start() = 0;
		virtual bool rotate(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) = 0;

	protected:
		//the vector of pointers to the four shape elements
		sf::Texture texture;
		std::vector<std::shared_ptr<TetrisShapeElement>> shapeElements;
};

