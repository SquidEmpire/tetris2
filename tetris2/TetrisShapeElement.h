#pragma once
#include "TetrisDrawable.h"
class TetrisShape; //forward declaration
class TetrisShapeElement : public TetrisDrawable, public std::enable_shared_from_this<TetrisShapeElement>
{
	public: 
		TetrisShapeElement(std::shared_ptr<TetrisShape> parent, sf::Color colour, sf::Texture &texture, int x, int y);
		bool isActive;
		int position[2];
		int nextValidPosition[2];
		std::shared_ptr<TetrisShape> parent;

		void draw(sf::RenderWindow& window);

		bool fall(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
		bool moveLeft(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
		bool moveRight(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);

		bool canFall(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
		bool canMoveLeft(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
		bool canMoveRight(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);

		bool canMoveToPositon(int x, int y, std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
		void moveToPosition(int x, int y, std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);

	private:
		sf::Color colour;
		sf::Texture texture;
		sf::Sprite sprite;
};

