#include "TetrisShapeL.h"

void TetrisShapeL::start() {
	std::shared_ptr<TetrisShape> sharedThis = shared_from_this();
	sf::Color colour = sf::Color(255, 136, 0);	

	rotation = 0;

	std::shared_ptr<TetrisShapeElement> shapepointer1(new TetrisShapeElement(sharedThis, colour, texture, position[0], position[1]));
	std::shared_ptr<TetrisShapeElement> shapepointer2(new TetrisShapeElement(sharedThis, colour, texture, position[0], position[1] + 1));
	std::shared_ptr<TetrisShapeElement> shapepointer3(new TetrisShapeElement(sharedThis, colour, texture, position[0], position[1] + 2));
	std::shared_ptr<TetrisShapeElement> shapepointer4(new TetrisShapeElement(sharedThis, colour, texture, position[0] + 1, position[1] + 2));

	shapeElements.push_back(shapepointer1);
	shapeElements.push_back(shapepointer2);
	shapeElements.push_back(shapepointer3);
	shapeElements.push_back(shapepointer4);
}

bool TetrisShapeL::rotate(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	bool didRotate;
	switch (rotation) {
		case 0:
			didRotate = rotateToPosition1(gameGrid);
			break;
		case 1:
			didRotate = rotateToPosition2(gameGrid);
			break;
		case 2:
			didRotate = rotateToPosition3(gameGrid);
			break;
		case 3:
			didRotate = rotateToPosition0(gameGrid);
			break;
		default: 
			didRotate = false;
	}
	return didRotate;
}

bool TetrisShapeL::rotateToPosition0(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	bool canRotate = true;
	int x = position[0];
	int y = position[1];
	canRotate = canRotate && shapeElements[0]->canMoveToPositon(x, y, gameGrid);
	canRotate = canRotate && shapeElements[1]->canMoveToPositon(x, y + 1, gameGrid);
	canRotate = canRotate && shapeElements[2]->canMoveToPositon(x, y + 2, gameGrid);
	canRotate = canRotate && shapeElements[3]->canMoveToPositon(x + 1, y + 2, gameGrid);

	if (canRotate) {
		//apply the rotation
		for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
			int x = shapeElement->nextValidPosition[0];
			int y = shapeElement->nextValidPosition[1];
			shapeElement->moveToPosition(x, y, gameGrid);
		}
		rotation = 0;
		return true;
	}
	return false;
}

bool TetrisShapeL::rotateToPosition1(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	bool canRotate = true;
	int x = position[0];
	int y = position[1];
	canRotate = canRotate && shapeElements[0]->canMoveToPositon(x, y, gameGrid);
	canRotate = canRotate && shapeElements[1]->canMoveToPositon(x + 1, y , gameGrid);
	canRotate = canRotate && shapeElements[2]->canMoveToPositon(x + 2, y, gameGrid);
	canRotate = canRotate && shapeElements[3]->canMoveToPositon(x, y + 1, gameGrid);

	if (canRotate) {
		//apply the rotation
		for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
			int x = shapeElement->nextValidPosition[0];
			int y = shapeElement->nextValidPosition[1];
			shapeElement->moveToPosition(x, y, gameGrid);
		}
		rotation = 1;
		return true;
	}
	return false;
}

bool TetrisShapeL::rotateToPosition2(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	bool canRotate = true;
	int x = position[0];
	int y = position[1];
	canRotate = canRotate && shapeElements[0]->canMoveToPositon(x, y, gameGrid);
	canRotate = canRotate && shapeElements[1]->canMoveToPositon(x + 1, y, gameGrid);
	canRotate = canRotate && shapeElements[2]->canMoveToPositon(x + 1, y + 1, gameGrid);
	canRotate = canRotate && shapeElements[3]->canMoveToPositon(x + 1, y + 2, gameGrid);

	if (canRotate) {
		//apply the rotation
		for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
			int x = shapeElement->nextValidPosition[0];
			int y = shapeElement->nextValidPosition[1];
			shapeElement->moveToPosition(x, y, gameGrid);
		}
		rotation = 2;
		return true;
	}
	return false;
}

bool TetrisShapeL::rotateToPosition3(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	bool canRotate = true;
	int x = position[0];
	int y = position[1];
	canRotate = canRotate && shapeElements[0]->canMoveToPositon(x, y + 1, gameGrid);
	canRotate = canRotate && shapeElements[1]->canMoveToPositon(x + 1, y + 1, gameGrid);
	canRotate = canRotate && shapeElements[2]->canMoveToPositon(x + 2, y + 1, gameGrid);
	canRotate = canRotate && shapeElements[3]->canMoveToPositon(x + 2, y, gameGrid);

	if (canRotate) {
		//apply the rotation
		for (std::shared_ptr<TetrisShapeElement> shapeElement : shapeElements) {
			int x = shapeElement->nextValidPosition[0];
			int y = shapeElement->nextValidPosition[1];
			shapeElement->moveToPosition(x, y, gameGrid);
		}
		rotation = 3;
		return true;
	}
	return false;
}