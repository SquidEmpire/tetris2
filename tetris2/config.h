#pragma once
class config {
    public:
        //the number of milliseconds between each automatic fall
        static const int gameSpeed = 500;        
        //window size
        static const int windowWidth = 300;
        static const int windowHeight = 650;
        //grid settings
        static const int gridRawWidth = 300; //grid size in pixels
        static const int gridRawHeight = 600;
        static const int gridLeft = 0;
        static const int gridTop = 50;
        //pixels per grid "unit"
        static const int gridSize = 30;
        static const int gridWidth = gridRawWidth / gridSize; //grid size in "units"
        static const int gridHeight = gridRawHeight / gridSize;
};

