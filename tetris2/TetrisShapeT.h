#pragma once
#include "config.h"
#include "TetrisShapeElement.h"
#include "TetrisShape.h"
class TetrisShapeT : public TetrisShape
{
public:
	using TetrisShape::TetrisShape;
	void start();
	bool rotate(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);

private:
	bool rotateToPosition0(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
	bool rotateToPosition1(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
	bool rotateToPosition2(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
	bool rotateToPosition3(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
};

