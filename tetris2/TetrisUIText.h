#pragma once
#include "TetrisDrawable.h"
class TetrisUIText : public TetrisDrawable
{
	public:
		TetrisUIText(sf::Font &font, std::string content, int size, int x, int y);
		void draw(sf::RenderWindow& window);
		void updateText(std::string content);
	private: 
		sf::Font font;
		sf::Text text;
		int position[2];
};

