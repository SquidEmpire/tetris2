#pragma once
#include "config.h"
#include "TetrisShapeElement.h"
#include "TetrisShape.h"
class TetrisShapeBox : public TetrisShape
{
public:
	using TetrisShape::TetrisShape;
	void start();
	bool rotate(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]);
};
