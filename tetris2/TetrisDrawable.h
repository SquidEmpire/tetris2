#pragma once
#include <SFML/Graphics.hpp>
class TetrisDrawable
{
	public: 
		virtual void draw(sf::RenderWindow& window) = 0;
};