#include <iostream>
#include <SFML/Graphics.hpp>
#include "config.h"
#include "TetrisShapeElement.h"

TetrisShapeElement::TetrisShapeElement(std::shared_ptr<TetrisShape> parent, sf::Color colour, sf::Texture &texture, int x, int y) {
	isActive = true;
	position[0] = x;
	position[1] = y;
	this->parent = parent;
	this->colour = colour;
	this->texture = texture;
	sprite.setTexture(texture);
	sprite.setColor(colour);
	sf::Vector2u temp = texture.getSize();
	float xScale = 1.0 * config::gridSize / temp.x;
	float yScale = 1.0 * config::gridSize / temp.y;
	sprite.setScale(sf::Vector2f(xScale, yScale));
}

void TetrisShapeElement::draw(sf::RenderWindow& window) {
	sprite.setPosition(position[0] * config::gridSize + config::gridLeft, position[1] * config::gridSize + config::gridTop);
	window.draw(sprite);
}

bool TetrisShapeElement::fall(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	int newYPosition = position[1] + 1;
	bool canMove = TetrisShapeElement::canMoveToPositon(position[0], newYPosition, gameGrid);
	if (canMove) {
		TetrisShapeElement::moveToPosition(position[0], newYPosition, gameGrid);
		return true;
	}
	//if we failed to fall deactivate the shape (we hit something)
	isActive = false;
	return false;
}

bool TetrisShapeElement::moveLeft(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	int newXPosition = position[0] - 1;
	bool canMove = TetrisShapeElement::canMoveToPositon(newXPosition, position[1], gameGrid);
	if (canMove) {
		TetrisShapeElement::moveToPosition(newXPosition, position[1], gameGrid);
		return true;
	}
	return false;
}

bool TetrisShapeElement::moveRight(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	int newXPosition = position[0] + 1;
	bool canMove = TetrisShapeElement::canMoveToPositon(newXPosition, position[1], gameGrid);
	if (canMove) {
		TetrisShapeElement::moveToPosition(newXPosition, position[1], gameGrid);
		return true;
	}
	return false;
}



bool TetrisShapeElement::canFall(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	int newYPosition = position[1] + 1;
	if (newYPosition < 0 || newYPosition >= config::gridHeight) {
		return false;
	}
	return TetrisShapeElement::canMoveToPositon(position[0], newYPosition, gameGrid);
}

bool TetrisShapeElement::canMoveRight(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	int newXPosition = position[0] + 1;
	if (newXPosition >= config::gridWidth) {
		return false;
	}
	return TetrisShapeElement::canMoveToPositon(newXPosition, position[1], gameGrid);
}

bool TetrisShapeElement::canMoveLeft(std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	int newXPosition = position[0] - 1;
	if (newXPosition < 0) {
		return false;
	}
	return TetrisShapeElement::canMoveToPositon(newXPosition, position[1], gameGrid);
}


//move the element to a new position. Also reset the next valid position of this element
void TetrisShapeElement::moveToPosition(int x, int y, std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	//negative x/y values, or x/y values outside the grid can't be checked
	if (x < 0 || y < 0 || x >= config::gridWidth || y >= config::gridHeight) {
		return;
	}
	//remove the exisiting pointer to this element where it currently is on the grid
	//the very first time this shape falls it begins offscreen (y = -1) and is not on the grid at all, so skip removing it
	//also , there's a chance another shapeElement has moved into this space in the time we started moving, so first check that we are actually
	//removing our own old pointer
	if (position[1] != -1 && gameGrid[position[0]][position[1]] == shared_from_this()) {
		gameGrid[position[0]][position[1]] = nullptr;
	}
	//add a pointer to the element where it will be next
	gameGrid[x][y] = shared_from_this();
	//apply the new position
	position[0] = x;
	position[1] = y;
	//reset the next valid position
	nextValidPosition[0] = x;
	nextValidPosition[1] = y;
}

//Check whether the element can be moved to a new position - also sets the next valid position of this element
bool TetrisShapeElement::canMoveToPositon(int x, int y, std::shared_ptr<TetrisShapeElement>(&gameGrid)[config::gridWidth][config::gridHeight]) {
	//negative x/y values, or x/y values outside the grid can't be checked
	if (x < 0 || y < 0 || x >= config::gridWidth || y >= config::gridHeight) {
		return false;
	}
	std::shared_ptr<TetrisShapeElement> newPositionInGrid = gameGrid[x][y];

	//check that the new position is in bounds
	if (y >= config::gridHeight) {
		return false;
	}
	if (x < 0) {
		return false;
	}
	if (x >= config::gridWidth) {
		return false;
	}
	//now check that the new position isn't already occupied
	if (newPositionInGrid != nullptr) {
		//if it is occupied, ignore that if the element is part of the same shape as this one
		if (newPositionInGrid->parent != this->parent) {			
			return false;
		}
	}

	nextValidPosition[0] = x;
	nextValidPosition[1] = y;

	return true;
}